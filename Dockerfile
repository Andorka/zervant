FROM clojure:openjdk-11-lein-2.9.1 as builder

WORKDIR /tmp/build
COPY . .
RUN lein uberjar

# clojure:openjdk-11-lein-2.9.1
FROM adoptopenjdk:11-hotspot-bionic as runner

LABEL maintainer="Andrew N Golovkov <andrew.golovkov@gmail.com>"

ARG version
ENV JARNAME=zervant-${version}-standalone.jar

WORKDIR /usr/local/bin
COPY --from=builder /tmp/build/config.yaml /etc/zervant.yaml
COPY --from=builder /tmp/build/target/uberjar/${JARNAME} /usr/local/bin/
CMD java -jar ${JARNAME} -c /etc/zervant.yaml
