(ns zervant.scheduler-test
  (:require [zervant.scheduler :as sut]
            [clojure.test :refer :all]
            [clojure.core.async :as async :refer [<!! >!! chan]]))

(deftest test-search-substring
  (testing "regular substring matches"
    (is (= true (sut/search-substring "testmeplease" "tmeple")))
    (is (= true (sut/search-substring "testmeplease" "")))
    (is (= true (sut/search-substring "" ""))))
  (testing "regular substring not matches"
    (is (= false (sut/search-substring "1234" "asd"))))
  (testing "not strings"
    (is (= false (sut/search-substring nil "asd")))
    (is (= false (sut/search-substring '(123 "456") "asd")))))

(deftest test-extract-name
  (testing "keyword with /"
    (is (= "http://google.com:80/search" (sut/extract-name :http://google.com:80/search))))
  (testing "keywith without /"
    (is (= "testmeplease:8080" (sut/extract-name :testmeplease:8080)))))

(deftest test-process-metric
  (testing "process successful request with failed search metric"
    (let [mchan (chan 1)]
      (>!! mchan {:job-name "testjob"
                  :latency 999
                  :found false
                  :opts {:url "http://testjob.io"}})
      (is (= (sut/process-metric mchan)
             {:job-name "testjob", :success true, :found false, :latency 999/1000}))))
  (testing "process successful request with successful search metric"
    (let [mchan (chan 1)]
      (>!! mchan {:job-name "testjob"
                  :latency 999
                  :found true
                  :opts {:url "http://testjob.io"}})
      (is (= (sut/process-metric mchan)
             {:job-name "testjob", :success true, :found true, :latency 999/1000}))))
  (testing "process failed request metric"
    (let [mchan (chan 1)]
      (>!! mchan {:job-name "testjob"
                  :latency 999
                  :found false
                  :opts {:url "http://testjob.io"}
                  :error (ex-info "some exception"
                                  {:causes #{:boom :damn}})})
      (is (= (sut/process-metric mchan)
             {:job-name "testjob", :success false, :found false, :latency 0})))))
