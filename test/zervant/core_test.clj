(ns zervant.core-test
  (:require [zervant.core :as sut]
            [clojure.test :refer :all]))

(deftest test-cli-options
  (testing "CLI Options"
    (is (= 3 (count sut/cli-options)))
    (is (= "-c" (-> sut/cli-options (nth 0) first)))
    (is (= "-m" (-> sut/cli-options (nth 1) first)))
    (is (= "-h" (-> sut/cli-options (nth 2) first)))))
