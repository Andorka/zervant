(ns zervant.config
  (:require [tupelo.parse.yaml :as yaml] ;; TODO remove yaml and use EDN
            [taoensso.timbre :as log]))

;; maybe atom is better here
(declare config)

(defn read-yaml-file [filename]
  "Just read the file as a YAML"
  (log/debug "Loading config file" filename)
  (let [data (yaml/parse (slurp filename))]
    (log/debug data)
    data))

(defn validate-config [config]
  "TODO: validate config schema"
  config)

(defn config-load [filename]
  "Load config values from config file"
  (->> filename
       read-yaml-file
       validate-config
       (def config)))
