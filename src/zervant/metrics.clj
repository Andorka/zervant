(ns zervant.metrics
  (:require [iapetos.core :as prometheus]
            [iapetos.export :as export]
            [iapetos.standalone :as standalone]
            [taoensso.timbre :as log]))

(def job-latency
  (prometheus/histogram
   :zervant/job-latency-seconds
   {:description "job execution latency in seconds by job name"
    :labels [:job_name]}))

(def job-last-request-success
  (prometheus/gauge
   :zervant/job-last-request-success
   {:description "last job request status by job name"
    :labels [:job_name]}))

(def job-last-request-string-found
  (prometheus/gauge
   :zervant/job-last-request-string-found
   {:description "last job request string found by job name"
    :labels [:job_name]}))

(def job-last-request-latency
  (prometheus/gauge
   :zervant/job-last-request-latency-seconds
   {:description "last job request latency in seconds by job_name"
    :labels [:job_name]}))

; "Register all our metrics in prometheus metrics registry"
(defonce registry
  (-> (prometheus/collector-registry)
      (prometheus/register
       job-latency
       job-last-request-success
       job-last-request-latency
       job-last-request-string-found)))

(defn measure-job-latency [job-name value]
  "update metrics with next measured values"
  (prometheus/observe (registry :zervant/job-latency-seconds
                                {:job_name job-name})
                      value))

(defn set-job-last-request-success [job-name value]
  "set 1 for successful job runs and 0 for unsuccessful"
  (prometheus/set (registry :zervant/job-last-request-success
                            {:job_name job-name})
                  (if value 1 0)))

(defn set-job-last-request-string-found [job-name value]
  "set 1 for successful job runs and 0 for unsuccessful"
  (prometheus/set (registry :zervant/job-last-request-string-found
                            {:job_name job-name})
                  (if value 1 0)))

(defn set-job-last-request-latency [job-name value]
  "set latest measured metrics"
  (prometheus/set (registry :zervant/job-last-request-latency-seconds
                            {:job_name job-name})
                  value))

(defn run-server
  [port]
  (log/info "Metrics server starts to listen on port" port)
  (standalone/metrics-server registry {:port port}))
