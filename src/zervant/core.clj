(ns zervant.core
  (:gen-class)
  (:refer-clojure :exclude [load])
  (:require [clojure.tools.cli :refer [parse-opts]]
            [zervant.config :as config]
            [zervant.scheduler :as scheduler]
            [zervant.metrics :as metrics]))

(def cli-options
  [["-c" "--config CONFIG"
    "Config file path"
    :default "config.yaml"
    :validate [#(.isFile (clojure.java.io/file %))
               "Can't find the config file"]]
   ["-m" "--metrics-port PORT"
    :default 8080
    :parse-fn #(Integer/parseInt %)
    :validate [#(< 0 % 0x10000) "Must be a number between 0 and 65536"]]
   ["-h" "--help"]])

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (let [opts (parse-opts args cli-options)
        _ (println opts)
        {:keys [options summary errors]} opts
        {:keys [config metrics-port help]} options]
    (when errors
      (println (clojure.string/join \newline errors))
      (println "Try '--help' for more information.")
      (System/exit 1))
    (when help
      (println "How to use this program:")
      (println summary)
      (System/exit 0))
    (config/config-load config)
    (metrics/run-server metrics-port)
    (scheduler/run config/config)))
