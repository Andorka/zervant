(ns zervant.scheduler
  (:require [clojure.core.async :as async :refer [>! <! <!! chan go thread]]
            [org.httpkit.client :as http] ;; probably not the best http library with current approach
            [zervant.metrics :as metrics]
            [clojure.string :as string]
            [taoensso.timbre :as log]))

(defn search-substring [data expect]
  "Search substring. Returns nil if body is not exists"
  (if (string? data)
    (string/includes? data expect)
    false))

(defn extract-name [n]
  "Black magic to convert keyword to string properly"
  (let [ns (namespace n)]
    (if ns
      (str ns "/" (name n))
      (name n))))

(defn run-one [metrics-chan check timeout interval]
  "Run thread to check one url for one phrase"
  (async/thread
    (let [job-name (extract-name (first check))
          {:keys [url expect]} (last check)]
      (log/debug "starting worker" job-name "with url" url "expecting" expect)
      (while true
        ;; example for failed response
        ;;
        ;; {:opts {:timeout 10000, :method :get, :url https://yaaaaa.ru}, :error #error {
        ;; :cause yaaaaa.ru: Name or service not known
        ;; :via
        ;; [{:type java.net.UnknownHostException
        ;;   :message yaaaaa.ru: Name or service not known
        ;;   :at [java.net.Inet6AddressImpl lookupAllHostAddr Inet6AddressImpl.java -2]}]
        (let [get (http/get url {:timeout timeout}) ;; create future
              start-time (System/currentTimeMillis)
              response @get ;; call the future
              latency (- (System/currentTimeMillis) start-time)
              found (search-substring (:body response) expect)
              sleep (->> start-time ;; calculate how long we should sleep
                         (- (System/currentTimeMillis))
                         (- interval))
              payload (merge
                       response
                       {:job-name job-name
                        :latency latency
                        :found found
                        :body nil
                        :headers nil})]

          ;; send the metrics asyncronously
          (go (>! metrics-chan payload))
          (if (pos? sleep)
            (Thread/sleep sleep)))))))

(defn process-metric [metrics-chan]
  "Metric processor. Calls metrics library"
  ;; extract variables from metric
  (let [m (<!! metrics-chan)
        {:keys [job-name found latency opts error]} m
        latency-ms (if-not error
                     (/ latency 1000)
                     0)]

    ;; format human-readable text
    (let [url (:url opts)
          success-text (if-not error "successful" "failed")
          result-text (cond
                        error (:cause (Throwable->map error))
                        found "expected string is found"
                        :else "expected string is not found")
          latency-text (->> latency-ms
                            (float)
                            (format "%.3f"))]
      (log/debug "Reguest to URL" url
                 "was" success-text
                 "with result" result-text
                 "with latency" latency-text))

    {:job-name job-name :success (nil? error) :found found :latency latency-ms}))

(defn set-metrics [metric]
  "Set metrics"
  (log/debug "Storing metrics" metric)
  (let [{:keys [job-name success found latency]} metric]
    (metrics/set-job-last-request-success job-name success)
    (metrics/set-job-last-request-string-found job-name found)
    (when success
      (metrics/set-job-last-request-latency job-name latency)
      (comment metrics/measure-job-latency job-name latency))))

(defn run [config]
  "Run the workers and metrics processor"
  (log/debug "Starting workers")
  (let [metrics-chan (chan 10)
        {:keys [timeout interval data]} config]

    ;; run the workers
    (doseq [x data]
      ;; expected value is in milliseconds
      (run-one metrics-chan x (* 1000 timeout) (* 1000 interval)))

    (log/debug "Starting metrics processor")
    (while true (set-metrics (process-metric metrics-chan)))))
