(defproject zervant "0.1.1-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :plugins [[lein-ancient "0.6.15"]
            [lein-cloverage "1.1.1"]]
  :dependencies [[org.clojure/clojure "1.10.1"]
                 [tupelo "0.9.145"] ;; for yaml library
                 [iapetos "0.1.8"] ;; prometheus metrics
                 ;; [prismatic/schema "1.1.9"] ;; schema validation
                 ;; [metosin/schema-tools "0.10.5"] ;; schema validation
                 [org.clojure/tools.cli "0.4.2"]
                 [org.clojure/core.async "0.4.500"]
                 ;; [clj-http "3.9.1"]
                 [http-kit "2.3.0"]
                 [com.taoensso/timbre "4.10.0"]] ;; logging
  :main ^:skip-aot zervant.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
